import math as math

import numpy as np
import pandas as pd
from sklearn.utils import shuffle as sf


def read_file(path_of_file, read_headers):
    if read_headers:
        return pd.read_csv(path_of_file)
    else:
        return pd.read_csv(path_of_file, header=None)


def fetch_human_observed_features():
    path_of_human_observed_feature_file = ""
    return read_file(path_of_human_observed_feature_file, True)


def fetch_all_data_sets():
    df_human_features = read_file("HumanObserved-Dataset/HumanObserved-Features-Data/HumanObserved-Features-Data.csv",
                                  True)
    df_human_features_same_pairs = read_file("HumanObserved-Dataset/HumanObserved-Features-Data/same_pairs.csv", False)
    df_human_features_different_pairs = read_file("HumanObserved-Dataset/HumanObserved-Features-Data/diffn_pairs.csv",
                                                  False)
    df_human_features_different_pairs = sf(df_human_features_different_pairs).iloc[0:2000:1]
    human_shuffled_data_frame = sf(
        pd.concat([df_human_features_same_pairs.iloc[1:], df_human_features_different_pairs.iloc[1:]],
                  ignore_index=True))

    human_subtracted_data_set, human_concatenated_data_set, human_target_data_set = \
        process_image_pairs(human_shuffled_data_frame, df_human_features)

    human_subtracted_training_data, human_subtracted_validation_data, human_subtracted_testing_data = \
        partition_data_set(human_subtracted_data_set, 80, 10, 10)
    human_concatenated_training_data, human_concatenated_validation_data, human_concatenated_testing_data = \
        partition_data_set(human_concatenated_data_set, 80, 10, 10)
    human_training_target, human_validation_target, human_testing_target = \
        partition_data_set(human_target_data_set, 80, 10, 10)

    df_gsc_features = read_file("GSC-Dataset/GSC-Features-Data/GSC-Features.csv", True)
    df_gsc_features_same_pairs = read_file("GSC-Dataset/GSC-Features-Data/same_pairs.csv", False)
    df_gsc_features_same_pairs = sf(df_gsc_features_same_pairs).iloc[0:1000:1]
    df_gsc_features_different_pairs = read_file("GSC-Dataset/GSC-Features-Data/diffn_pairs.csv", False)
    df_gsc_features_different_pairs = sf(df_gsc_features_different_pairs).iloc[0:2000:1]
    gsc_shuffled_data_frame = sf(
        pd.concat([df_gsc_features_same_pairs, df_gsc_features_different_pairs], ignore_index=True))

    gsc_subtracted_data_set, gsc_concatenated_data_set, gsc_target_data_set = \
        process_image_pairs(gsc_shuffled_data_frame, df_gsc_features)

    gsc_subtracted_training_data, gsc_subtracted_validation_data, gsc_subtracted_testing_data = \
        partition_data_set(gsc_subtracted_data_set, 80, 10, 10)
    gsc_concatenated_training_data, gsc_concatenated_validation_data, gsc_concatenated_testing_data = \
        partition_data_set(gsc_concatenated_data_set, 80, 10, 10)
    gsc_training_target, gsc_validation_target, gsc_testing_target = \
        partition_data_set(gsc_target_data_set, 80, 10, 10)

    return human_subtracted_training_data, human_subtracted_validation_data, human_subtracted_testing_data, \
           human_concatenated_training_data, human_concatenated_validation_data, human_concatenated_testing_data, \
           human_training_target, human_validation_target, human_testing_target, \
           gsc_subtracted_training_data, gsc_subtracted_validation_data, gsc_subtracted_testing_data, \
           gsc_concatenated_training_data, gsc_concatenated_validation_data, gsc_concatenated_testing_data, \
           gsc_training_target, gsc_validation_target, gsc_testing_target


def process_image_pairs(image_pairs, feature_set):
    image_pairs_subtracted_features = []
    image_pairs_concatenated_features = []
    image_pairs_target_values = []
    image_pairs = image_pairs.values
    for image_pair_index in range(0, len(image_pairs)):
        image_pairs_subtracted_features.append(
            subtract_features_of_two_images(image_pairs[image_pair_index][0], image_pairs[image_pair_index][1],
                                            feature_set))
        image_pairs_concatenated_features.append(
            concatenate_features_of_two_images(image_pairs[image_pair_index][0], image_pairs[image_pair_index][1],
                                               feature_set))
        image_pairs_target_values.append(image_pairs[image_pair_index][2])
        print("Image pair {0} subtracted and concatenated".format(image_pair_index))

    return np.array(image_pairs_subtracted_features), np.array(
        image_pairs_concatenated_features), np.reshape(np.array(image_pairs_target_values), (-1, 1)).astype(int)


def subtract_features_of_two_images(image_1_id, image_2_id, feature_set_data_frame):
    df = feature_set_data_frame.set_index(["img_id"])
    return df.loc[image_1_id].values[1:] - df.loc[image_2_id].values[1:]


def concatenate_features_of_two_images(image_1_id, image_2_id, feature_set_data_frame):
    df = feature_set_data_frame.set_index(["img_id"])
    return np.concatenate((df.loc[image_1_id].values[1:], df.loc[image_2_id].values[1:]), axis=0)


def get_percentage_of_data_after_skipping_elements(data_to_be_partitioned, partition_percent, items_to_skip):
    length_of_data = len(data_to_be_partitioned)
    partition_length = math.floor(0.01 * partition_percent * length_of_data)
    return data_to_be_partitioned[items_to_skip: items_to_skip + partition_length]


def get_human_observed_data_set(same_image_pairs, diff_image_pairs, features):
    image_pairs = sf(same_image_pairs.append(diff_image_pairs, ignore_index=True))
    return process_image_pairs(image_pairs, features)


def get_gsc_observed_data_set(same_image_pairs, diff_image_pairs, features):
    image_pairs = sf(same_image_pairs.append(diff_image_pairs, ignore_index=True))
    return process_image_pairs(image_pairs, features)


def partition_data_set(data_set, training_percent, validation_percent, testing_percent):
    training_set = get_percentage_of_data_after_skipping_elements(data_set, training_percent, 0)
    validation_set = get_percentage_of_data_after_skipping_elements(data_set, validation_percent,
                                                                    len(training_set))
    testing_set = get_percentage_of_data_after_skipping_elements(data_set, testing_percent,
                                                                 len(training_set) + len(validation_set))
    return training_set, validation_set, testing_set
