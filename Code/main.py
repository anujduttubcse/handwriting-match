import data_preprocessor as dp


def main():
    human_subtracted_training_data, human_subtracted_validation_data, human_subtracted_testing_data, \
    human_concatenated_training_data, human_concatenated_validation_data, human_concatenated_testing_data, \
    human_training_target, human_validation_target, human_testing_target, \
    gsc_subtracted_training_data, gsc_subtracted_validation_data, gsc_subtracted_testing_data, \
    gsc_concatenated_training_data, gsc_concatenated_validation_data, gsc_concatenated_testing_data, \
    gsc_training_target, gsc_validation_target, gsc_testing_target = dp.fetch_all_data_sets()

    print("Fetched and formatted all data")

    # dff = dp.read_file("HumanObserved-Dataset/HumanObserved-Features-Data/HumanObserved-Features-Data.csv", True)
    # dfsp = dp.read_file("HumanObserved-Dataset/HumanObserved-Features-Data/same_pairs.csv", False)
    # dfdp = dp.read_file("HumanObserved-Dataset/HumanObserved-Features-Data/diffn_pairs.csv", False)
    # dfdp = sf(dfdp).iloc[0:2000:1]
    # print(dfdp)
    # print(dfsp.append(dfdp))
    # image_pairs_subtracted_features, image_pairs_concatenated_features, image_pairs_target_values = \
    #     dp.process_image_pairs(dfdp, dff)
    # training_set = dp.get_percentage_of_data_after_skipping_elements(image_pairs_subtracted_features, 80, 0)
    # validation_set = dp.get_percentage_of_data_after_skipping_elements(image_pairs_subtracted_features, 10,
    #                                                                    len(training_set))
    # testing_set = dp.get_percentage_of_data_after_skipping_elements(image_pairs_subtracted_features, 10,
    #                                                                 len(training_set) + len(validation_set))
    # print("Length of data set: {0}".format(len(image_pairs_subtracted_features)))
    # print("Length of training set: {0}".format(len(training_set)))
    # print("Length of validation set: {0}".format(len(validation_set)))
    # print("Length of testing set: {0}".format(len(testing_set)))
    # print("Length of the 3 sets: {0}".format(len(training_set) + len(validation_set) + len(testing_set)))
    #
    # print(image_pairs_concatenated_features)
    # print(image_pairs_target_values)


if __name__ == '__main__':
    main()
